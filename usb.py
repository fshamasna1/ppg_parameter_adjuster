import serial
import time
import serial.tools.list_ports

class USB:

    def __init__(self, *, baudrate=115200):
        self.VENDOR_ID_PRODUCT_ID = '10C4:0008'
        self.connection = serial.Serial()
        self.baudrate = baudrate
        self.port = self.get_COM_port()
        self.connection.baudrate = self.baudrate
        self.connection.port = self.port
        self.connection.open()

    def send_over_usb(self, chunk):
        time.sleep(0.000001)
        self.connection.write(chunk)
        data = self.connection.read(len(chunk))
        # print(data)

    def read_from_usb(self, size):
        return self.connection.read(size)

    def readline_from_usb(self):
        return self.connection.readline()

    def get_COM_port(self):
        ports = serial.tools.list_ports.comports()
        for port, desc, hwid in sorted(ports):
            if self.VENDOR_ID_PRODUCT_ID in hwid:
                return port



