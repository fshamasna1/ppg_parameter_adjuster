pressure_schema = {
    "type": "object",
    "properties": {
        "timestamp": {
            "type": "integer",
        },
        "value": {
            "type": "integer",
            "minimum": 0
        },
        "value_in_pa": {
            "type": "integer",
            "minimum": 0
        }
    },
    "required": ["timestamp", "value", "value_in_pa"]
}

battery_schema = {
    "type": "object",
    "properties": {
        "timestamp": {
            "type": "integer",
        },
        "current": {
            "type": "integer",
        },
        "voltage": {
            "type": "integer",
        },
        "temperature": {
            "type": "integer",
        },
        "soc": {
            "type": "integer",
        }
    },
    "required": ["timestamp", "current", "voltage", "temperature", "soc"]
}

hr_spo2_schema = {
    "type": "object",
    "properties": {
        "timestamp": {
            "type": "integer",
        },
        "greenCnt": {
            "type": "integer",
            "minimum": 0
        },
        "irCnt": {
            "type": "integer",
            "minimum": 0
        },
        "redCnt": {
            "type": "integer",
            "minimum": 0
        },
        "hr": {
            "type": "integer",
            "minimum": 0
        },
        "hr_conf": {
            "type": "integer",
            "minimum": 0
        },
        "spo2": {
            "type": "integer",
            "minimum": 0
        },
        "spo2_conf": {
            "type": "integer",
            "minimum": 0
        }
    },
    "required": ["greenCnt", "irCnt", "redCnt", "hr", "hr_conf", "spo2", "spo2_conf"]
}

raw_schema = {
    "type": "object",
    "properties": {
        "led1": {
            "type": "integer",
        },
        "led2": {
            "type": "integer",
            "minimum": 0
        },
        "led3": {
            "type": "integer",
            "minimum": 0
        },
        "led4": {
            "type": "integer",
            "minimum": 0
        },
        "led5": {
            "type": "integer",
            "minimum": 0
        },
        "led6": {
            "type": "integer",
            "minimum": 0
        }
    },
    "required": ["led1", "led2", "led3", "led4", "led5", "led6"]
}

# select "redCnt" from "hr_spo2" where time < now();
# http://138.68.82.201:3003/d/SKsmsz_Gz/maxim-hub?orgId=1&refresh=5s
