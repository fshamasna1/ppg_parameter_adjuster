from flash import *
from usb import *
from maxim_config_data import *

CURRENT_31_SCALE = 31.0
CURRENT_62_SCALE = 62.0
CURRENT_93_SCALE = 93.0
CURRENT_124_SCALE = 124.0
CURRENT_MAX_BITS = 256


def set_freq_avg_sampling_conf(freq, avg_smp):
    freq_bits = PPG_FREQUENCY[freq]
    avg_smp_bits = PPG_AVERAGE_SAMPLING[avg_smp]
    return freq_bits << 3 | avg_smp_bits


def integration_time(ppg_tint_val, ppg1_adc, ppg2_adc):
    return (PPG2_GAIN_RANGE_CONTROL[ppg2_adc] << 4) | (PPG1_GAIN_RANGE_CONTROL[ppg1_adc] << 2) | PULSE_WIDTH[
        ppg_tint_val]


def set_full_current_range(led1, led2, led3):
    return (CURRENT_SCALE_RANGE[led3] << 4) | (CURRENT_SCALE_RANGE[led2] << 2) | CURRENT_SCALE_RANGE[led1]


def set_led_current(value):
    div = 0
    if CURRENT_31_SCALE >= value >= 0:
        div = (int(value / 0.12))
    elif CURRENT_62_SCALE >= value >= 0:
        div = (int(value / 0.24))
    elif CURRENT_93_SCALE >= value >= 0:
        div = (int(value / 0.36))
    elif CURRENT_124_SCALE >= value >= 0:
        div = (int(value / 0.48))
    if div > CURRENT_MAX_BITS:
        div = CURRENT_MAX_BITS
    return div


def set_led_current_fixed_range(value):
    div = value // 0.48
    if div > CURRENT_MAX_BITS:
        div = CURRENT_MAX_BITS
    return div


ALGORITHM_MODE = 1
RAW_MODE = 0

if __name__ == '__main__':
    frequency = 4096
    avg_sampling = 64
    ADC_range_of_the_SPO2_sensor_PPG1 = 32768
    ADC_range_of_the_SPO2_sensor_PPG2 = 32768
    pulse_width = 117.3
    current_range_scale_led1 = 124
    current_range_scale_led2 = 124
    current_range_scale_led3 = 124
    led_1_current = 5
    led_2_current = 5
    led_3_current = 5
    mode = "RAW"

    usb = USB()
    flash = Flash(usb)

    if mode == "RAW":
        flash.write_to_flash("CONFIG_MAXIM_MODE", RAW_MODE)
        MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_FREQ_AVG_SAMPLING"] = set_freq_avg_sampling_conf(frequency, avg_sampling)
        print((MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_FREQ_AVG_SAMPLING"]))
        MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_INTEGRATION_TIME"] = integration_time(pulse_width,
                                                                               ADC_range_of_the_SPO2_sensor_PPG1,
                                                                               ADC_range_of_the_SPO2_sensor_PPG2)
        print((MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_INTEGRATION_TIME"]))
        MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_FULL_CURRENT_RANGE"] = set_full_current_range(current_range_scale_led1,
                                                                                       current_range_scale_led2,
                                                                                       current_range_scale_led3)
        print((MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_FULL_CURRENT_RANGE"]))
        MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_LED_1_CURRENT"] = set_led_current_fixed_range(led_1_current)
        print((MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_LED_1_CURRENT"]))
        MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_LED_2_CURRENT"] = set_led_current_fixed_range(led_2_current)
        print((MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_LED_2_CURRENT"]))
        MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_LED_3_CURRENT"] = set_led_current_fixed_range(led_3_current)
        print((MAXIM_MAIN_CONFIG["CONFIG_RAWPPG_LED_3_CURRENT"]))
        # write the new configuration
        [flash.write_to_flash(key, value) for key, value in MAXIM_MAIN_CONFIG.items()]
    else:
        flash.write_to_flash("CONFIG_MAXIM_MODE", ALGORITHM_MODE)

    flash.device_restart()
