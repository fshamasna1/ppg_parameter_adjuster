from usb import *
from influxDB import *
import sensors_schema
from json import loads, dumps
from jsonschema import Draft4Validator
import datetime


# select "predicted" from "ECG" where time < now();
def data_preprocessing(raw_data):
    sensor_type_idx = 0
    decoded_data = raw_data.decode()[:-1]  # remove the \n
    received_list = decoded_data.split(',')
    return received_list[sensor_type_idx], received_list[1:]


def validate_measurements(values, schema):
    measurements_type = schema["properties"].keys()
    data_dict = {
        measurement_type: int(value)
        for measurement_type, value in zip(measurements_type, values)
    }
    data_json = dumps(data_dict)

    # Validation
    validator = Draft4Validator(schema)
    for error in validator.iter_errors(loads(data_json)):
        print(error, end='\n-----------------------\n')

    return data_dict


if __name__ == '__main__':
    database_name = "DB1"
    host_name = '138.68.82.201'
    port = 8086
    usb = USB()
    # database = InfluxBD(database_name=database_name, host_name=host_name, port=port)

    while True:
        sensors_schemas = {
            "raw_maxim": sensors_schema.raw_schema
        }

        raw_data = usb.readline_from_usb()
        sensor_type, sensor_values = data_preprocessing(raw_data)
        data = validate_measurements(sensor_values, sensors_schemas[sensor_type])
        measurement = Measurement(measurement=sensor_type,
                                  tags={"device": "87:98:4a:7f:9b"},
                                  time=datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'),
                                  fields=data
                                  )
        print(measurement.data_point())
        # database.store_measurement(measurement.data_point())
