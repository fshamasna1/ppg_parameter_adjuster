from influxdb import InfluxDBClient


class Measurement:
    def __init__(self, measurement, tags, time, fields):
        self.measurement = measurement
        self.tags = tags
        self.time = time
        self.fields = fields

    def data_point(self):
        return {
            "measurement": self.measurement,
            "tags": self.tags,
            "time": self.time,
            "fields": self.fields
        }


class InfluxBD:

    def __init__(self, database_name, host_name='localhost', port=8086):
        self.client = InfluxDBClient(host=host_name, port=port)
        # self.client.create_database(database_name)
        self.client.switch_database(database_name)

    def store_measurement(self, value):
        self.client.write_points([value])
