class Flash:
    def __init__(self, usb, size_of_response=6, success_response=b'PASSED', failed_response=b'FAILED'):
        self.usb = usb
        self.size_of_response = size_of_response
        self.success_response = success_response
        self.failed_response = failed_response
        self.response = None

    def send_data(self, command):
        self.usb.send_over_usb(command)
        self.response = self.usb.read_from_usb(self.size_of_response)
        # print(self.response)

    def write_to_flash(self, key, value):
        command = 'write_to_flash_enable'+'\0'
        self.send_data(self.encode_data(command))
        key_val = str(key) + ':' + str(value) + '\0'
        self.send_data(self.encode_data(key_val))

    def read_from_flash(self, key):
        command = 'read_from_flash'+'\0'
        self.send_data(self.encode_data(command))
        key = key + '\0'
        self.usb.send_over_usb(self.encode_data(key))
        raw_data = self.usb.readline_from_usb()
        return raw_data

    def device_restart(self):
        print('the device has been restarted!')
        command = 'system_restart' + '\0'
        self.send_data(self.encode_data(command))

    @classmethod
    def encode_data(cls, data):
        return str.encode(str(data))